# chr-rs

A Borland CHR font parser and converter---work in progress!

## Building

`cargo build --release`

## Usage

`./target/release/chr show filename.chr`---gives a verbose printout of what the parser found

`./target/release/chr convert filename.chr`---TODO hopefully this will convert the input file into `filename.ttf` or `filename.otf`
