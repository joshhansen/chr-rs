use nom::number::complete::le_i8;
use nom::multi::many_till;
use nom::multi::count;
use nom::number::complete::le_u8;
use nom::bytes::complete::take;
use nom::number::complete::le_i16;
use nom::error::{Error, ErrorKind};
use nom::Err;
use nom::bytes::complete::take_until;
use nom::bytes::complete::tag;
use nom::IResult;

#[derive(Debug)]
struct Header {
    description: String,
    header_size: i16,
    font_id: String,
    data_size: i16,
    major_version: i16,
    minor_version: i16,
}

static SIGNATURE: [u8; 4] = [0x50, 0x4B, 0x08, 0x08];
static DESC_TERM: [u8; 1] = [0x1A];
static STROKE_CHECK: [u8; 1] = [0x2B];
static NULL: [u8; 1] = [0x00];

fn parse_header(i: &[u8]) -> IResult<&[u8], Header> {
  let (i, _) = tag(SIGNATURE)(i)?;

  let (i, description) = take_until(&DESC_TERM[..])(i)?;

  let description = std::str::from_utf8(description)
    .map_err(|_| Err::Failure(Error::new(i, ErrorKind::Fail)))
  ?;

  let description = description.to_owned();

  // Consume the description terminator
  let (i, _) = tag(DESC_TERM)(i)?;

  let (i, header_size) = le_i16(i)?;

  let (i, font_id) = take(4_usize)(i)?;

  let (i, data_size) = le_i16(i)?;

  let (i, major_version) = le_i16(i)?;

  let (i, minor_version) = le_i16(i)?;

  let len_so_far: i16 = 4// signature
  + description.len() as i16
  + 1// desc terminator
  + 2//header size
  + 4//font id
  + 2//data size
  + 2//major version
  + 2//minor version
  ;

  let header_remainder = header_size - len_so_far;

  println!("header_remainder: {}", header_remainder);

  let (i, _) = count(tag(NULL), header_remainder as usize)(i)?;// skip padding nulls

//   let (i, _) = take(header_remainder as usize)(i)?;

  let font_id_bytes: Vec<u8> = font_id[0..4].try_into().unwrap();// unwrap OK because we took 4 bytes exactly

  Ok((i, Header {
    description,
    header_size,
    font_id: String::from_utf8(font_id_bytes).expect("font_id unexpectedly contained non-utf8 bytes"),
    data_size,
    major_version,
    minor_version,
  }))
}

#[derive(Debug)]
struct StrokeHeader {
    character_count: i16,
    starting_character: char,
    strokes_offset: i16,
    origin_to_ascender: i8,
    origin_to_baseline: i8,
    origin_to_descender: i8,
    character_stroke_offsets: Vec<i16>,
    character_widths: Vec<u8>,
}

fn parse_stroke_header(i: &[u8], header_size: i16) -> IResult<&[u8], StrokeHeader> {
    let (i, _) = tag(STROKE_CHECK)(i)?;

    let (i, character_count) = le_i16(i)?;

    let (i, _) = take(1usize)(i)?;// skip the undefined byte

    let (i, starting_character) = le_i8(i)?;

    let (i, strokes_offset) = le_i16(i)?;

    let (i, _) = take(1usize)(i)?;// "scan flag"

    let (i, origin_to_ascender) = le_i8(i)?;

    let (i, origin_to_baseline) = le_i8(i)?;

    let (i, origin_to_descender) = le_i8(i)?;

    let null_count = 16 - (header_size as usize
      + 1// stroke check
      + 2// character count
      + 1// undefined byte
      + 1// starting character
      + 2// stroke offset
      + 1// scan flag
      + 1// origin to ascender
      + 1// origin to baseline
      + 1// origin to descender
    ) % 16;

    println!("null_count: {}", null_count);

    let (i, _) = count(tag(NULL), null_count)(i)?;// pad to the nearest 16-byte chunk

    let (i, character_stroke_offsets) = count(le_i16, character_count as usize)(i)?;

    let (i, character_widths) = count(le_u8, character_count as usize)(i)?;//???

    Ok((i, StrokeHeader {
        character_count,
        starting_character: char::from(starting_character as u8),
        strokes_offset,
        origin_to_ascender,
        origin_to_baseline,
        origin_to_descender,
        character_stroke_offsets,
        character_widths,
    }))
}



struct StrokeDimension(bool, i8);

fn parse_stroke_dimension(i: &[u8], require_opcode: bool) -> IResult<&[u8], StrokeDimension> {
    let (i, x) = take(1usize)(i)?;

    let c = x[0];

    let opcode = c & 0b10000000 > 0;// leading bit is the opcode

    if require_opcode {
        if !opcode {
            return Err(Err::Error(Error::new(i, ErrorKind::Fail)));
        }
    }

    let sign: isize = if c & 0b01000000 > 0 { -1 } else { 1 };// second bit is the sign

    let raw_value = (c & 0b00111111) as isize;// remaining are the value

    let value = (sign * raw_value) as i8;

    Ok((i, StrokeDimension(opcode, value)))
}

#[derive(Debug)]
pub enum Instruction {
    EndOfCharacterStrokeDefinition,
    DoScan,
    MovePlotterToCoords,
    DrawLineAndMovePlotterToCoords,
}
impl From<(bool,bool)> for Instruction {
    fn from(xy_opcodes: (bool, bool)) -> Instruction {
        match (xy_opcodes.0, xy_opcodes.0) {
            (false, false) => Instruction::EndOfCharacterStrokeDefinition,
            (false, true) => Instruction::DoScan,
            (true, false) => Instruction::MovePlotterToCoords,
            (true, true) => Instruction::DrawLineAndMovePlotterToCoords,
        }
    }
}

#[derive(Debug)]
pub struct Stroke {
    pub x: i8,
    pub y: i8,
    pub instruction: Instruction,
}

fn parse_stroke(i: &[u8]) -> IResult<&[u8], Stroke> {
    let (i, x_instruction) = parse_stroke_dimension(i, false)?;
    let (i, y_instruction) = parse_stroke_dimension(i, false)?;

    let instruction = Instruction::from((x_instruction.0, y_instruction.0));

    Ok((i, Stroke { x: x_instruction.1, y: y_instruction.1, instruction }))
}

fn parse_end_stroke(i: &[u8]) -> IResult<&[u8], Stroke> {
    let (i, x_instruction) = parse_stroke_dimension(i, true)?;
    let (i, y_instruction) = parse_stroke_dimension(i, true)?;

    let instruction = Instruction::from((x_instruction.0, y_instruction.0));

    Ok((i, Stroke { x: x_instruction.1, y: y_instruction.1, instruction }))
}

pub type Character = Vec<Stroke>;

fn parse_character(i: &[u8]) -> IResult<&[u8], Character> {
    let (i, strokes) = many_till(parse_stroke, parse_end_stroke)(i)?;
    Ok((i, strokes.0))
}

#[derive(Debug)]
pub struct ChrFont {
    pub characters: Vec<Character>,
}

pub fn parse_chr(i: &[u8]) -> IResult<&[u8], ChrFont> {
    let (j, header) = parse_header(i)?;

    println!("header: {:?}", header);

    let (_, stroke_header) = parse_stroke_header(j, header.header_size)?;

    println!("stroke_header: {:?}", stroke_header);

    let mut characters: Vec<Character> = Vec::with_capacity(stroke_header.character_count as usize);

    for char_stroke_offset in stroke_header.character_stroke_offsets
        .iter().take(stroke_header.character_count as usize - 1// skip the last character which always seems to be degenerate---just two null bytes
    ) {
        let char_stroke_position = (header.header_size + stroke_header.strokes_offset + char_stroke_offset) as usize;

        println!("char_stroke_positon: {}", char_stroke_position);

        let (_, character) = parse_character(&i[char_stroke_position..])?;

        println!("character: {:?}", character);

        characters.push(character);
    }

    Ok((i, ChrFont{
        characters,
    }))
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
