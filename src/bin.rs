use chr::parse_chr;
use std::fs::File;
use std::io::Read;
use clap::Parser;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
   #[clap(subcommand)]
   command: Command,
}

#[derive(Debug, clap::Subcommand)]
enum Command {
   Show {
    #[clap()]
    path: String,
   },
   Convert,
}

fn main() -> std::io::Result<()> {
    let path = std::env::current_dir()?;
    println!("The current directory is {}", path.display());


   let args = Args::parse();

   match &args.command {
        Command::Show { path } => {
            println!("show {}", path);

            let mut f = File::open(path)?;

            let mut buffer = Vec::new();
            f.read_to_end(&mut buffer)?;

            let result = parse_chr(&buffer[..]);

            if let Ok(font) = result {
                println!("{:?}", font);
            } else if let Err(err) = result {
                eprintln!("Error parsing font: {}", err);
            }

            Ok(())
        },
        Command::Convert => {
            println!("convert");
            Ok(())
        }
    }
}
